<?php

require_once __DIR__ . '/../src/RouterInterface.php';
require_once __DIR__ . '/../src/Regex.php';
require_once __DIR__ . '/../src/Exception.php';

$routes = array(
    array(
        'node', // Route name, this is used for reverse routing.
        array('GET', '^/node/(\d+)/$', array('id')), // Http method, regex pattern, captured parameter keys
        array('\Foo\Bar\Baz', 'node') // Class and method name.
    ),
    array(
        'article',
        array('GET', '^/article/([a-z0-9-]+)/$', array('slug')),
        array('\Foo\Bar\Bat', 'article')
    ),
);

$regexRouter = new Burntant\Router\Regex($routes);

$paths = array(
    'node' => 'GET',
    'node/23' => 'GET',
    '/node/23' => 'GET',
    '/node/23/' => 'GET',
    '/node/47/' => 'PUT',
    '/node/hello/' => 'GET',
    '/article/hello-earth/' => 'GET'

);

// Loop through the paths defined above and see if we can match them against registered routes:
foreach ($paths as $path => $http_method) {
    $result = $regexRouter->resolve($path, $http_method);
    var_dump($result);
}

// To output a path using route name and parameters:
echo $url = $regexRouter->pathFor('node', array('23')) . "\n";
echo $url = $regexRouter->pathFor('article', array('humpty-dumpty-sat-on-the-wall')) . "\n";