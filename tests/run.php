<?php

//TODO: Implement assert_handler, or use Phpunit.
use Burntant\Router\Test\RegexTest;

require_once __DIR__ . '/RegexTest.php';
require_once __DIR__ . '/../src/RouterInterface.php';
require_once __DIR__ . '/../src/Exception.php';
require_once __DIR__ . '/../src/Regex.php';

$regexTest = new RegexTest;
$reflector = new ReflectionClass($regexTest);
$tests_run = 0;
foreach($reflector->getMethods() as $method) {
    if(strpos($method->name, 'test') === 0) {
        call_user_method($method->name, $regexTest);
        $tests_run++;
    }
}

printf('Ran %d tests.', $tests_run);