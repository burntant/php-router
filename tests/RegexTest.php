<?php

namespace Burntant\Router\Test;

use Burntant\Router\Regex as RegexRouter;

class RegexTest
{
    protected $routes;

    protected $paths;

    public function __construct()
    {
        $this->routes = array(
            array(
                'node', // Route name, used for reverse routing
                array('GET', '^/node/(\d+)/$', array('id')), // Http method, regex, captured param keys
                array('\Foo\Bar\Baz', 'node') // Class and Method name.
            ),
            array(
                'article',
                array('GET', '^/article/([a-z0-9-]+)/$', array('slug')),
                array('\Foo\Bar\Bat', 'article')
            ),
            array(
                'news',
                array('GET', '^/news/(\d+)/(\d+)/(\d+)/$', array('year', 'month', 'day')),
                array('\Foo\Bar\Zot', 'story')
            )
        );
        $this->paths = array(
            'node' => 'GET',
            'node/23' => 'GET',
            '/node/23' => 'GET',
            '/node/23/' => 'GET',
            '/node/47/' => 'PUT',
            '/node/hello/' => 'GET',
            '/article/hello-earth/' => 'GET',
            '/news/2015/' => 'GET',
            '/news/2012/12/31/' => 'GET'
        );

        $this->router = new RegexRouter($this->routes);
    }

    public function __destruct() {
        unset($this->routes, $this->paths);
    }

    public function testResolve()
    {
        assert(
            array('\Foo\Bar\Bat', 'article', array('slug' => 'hello-earth'))
            === $this->router->resolve('/article/hello-earth/', 'GET')
        );
        assert(
            array('\Foo\Bar\Zot', 'story', array('year' => '2012', 'month' => '12', 'day' => '31'))
            === $this->router->resolve('/news/2012/12/31/', 'GET')
        );
    }

    public function testPathFors()
    {
        $routes = array(
            array('node', array('47'), '/node/47/'),
            array('article', array('hello-earth'), '/article/hello-earth/'),
            array('news', array('2015', '12', '31'), '/news/2015/12/31/')
        );

        foreach ($routes as $route) {
            assert(
                $this->router->pathFor($route[0], $route[1])
                == $route[2]
            );
        }
    }
}