Name:
    Php Router

Copyright:
    Grant Burton 2015.

Summary:
    A basic router that associates web paths to class and method names and matched params.

Why:
    I wanted to use basic regex matching on web paths for web application routing.
    Good for a handful of route patterns.

Features:
    Match a route based on an application path and http method.
    A match will return an array consisting of a class name, a method and any matched params.

Caveats:
    There are likely better routers out there (perhaps use one of them).
    Features may be added or removed at any time.
    Don't expect support.

Docs:
    Create your router by passing a route configuration into the constructor.

    Then try and match your route:

    if($match = $router->resolve('/foo/bar', 'GET')) {
        list($class_name, $method, $params) = $match;
    }

    See examples directory.

    I have taken the matched params and passed them through as controller method arguments.  But dispatch how you like.

Tests:
    TODO.

Provided:
    As is.

TODO:
    Router\Regex::urlFor
        Params are substituted in matched order.  Currently named params will not work for reverse routing.

License:
    Php Router is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Php Router is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Php Dic.  If not, see <http://www.gnu.org/licenses/>.