<?php

namespace Burntant\Router;

interface RouterInterface
{
    // Given a route name and optional params, return application path
    public function pathFor($name, $params);
    // Return array(class_name, method_name, params)
    public function resolve($app_path, $http_method);
}