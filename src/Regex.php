<?php
namespace Burntant\Router;

/**
 * Class responsibility:
 * Match application path to a class, method and parameters.
 *
 * Requires route configuration in array:
 *
 *          array(
 *              'node', // Route name, used for reverse routing
 *              array('GET', '^/node/(\d+)/$', array('id')), // Http method, regex, captured param keys
 *              array('\Foo\Bar\Baz', 'bat') // Class and Method name.
 *          )
 *
 */
class Regex implements RouterInterface
{
    protected $routes;

    protected $mapPatternRoute;

    protected $mapNameRoute;

    protected $mapRouteMethod;

    public $methods = array(
        'GET', 'HEAD', 'POST', 'PUT', 'DELETE'
        );

    public function  __construct($routes)
    {
        $this->routes = $routes;
        $this->indexRoutes();
    }
    protected function indexRoutes()
    {
        foreach($this->routes as $key => $route) {
            $this->mapNameRoute[$route[0]] = $key;
            $this->mapPatternRoute[$route[1][1]] = $key;
            if(!in_array($route[1][0], $this->methods)) {
                throw new Exception('Invalid HTTP method assigned in route configuration.');
            }
            $this->mapRouteMethod[$key] = $route[1][0];
        }
    }
    public function getRouteFromName($name)
    {
        $route_key = $this->mapNameRoute[$name];

        return $this->routes[$route_key];
    }
    /**
     * Returns: array(class_name, method_name, params)
     */
    public function resolve($app_path, $http_method)
    {
        $params = array();
        $matched = false;

        // Loop through each pattern until we find a match
        foreach($this->mapPatternRoute as $pattern => $route_key) {
            $pattern = '@' . $pattern . '@';
            if(
                $this->mapRouteMethod[$route_key] == $http_method
                && preg_match_all($pattern, $app_path, $matches, PREG_SET_ORDER))
            {
                $matched = true;
                break;
            }
        }

        if(! $matched ) return false;

        $route = $this->routes[$route_key]; 

        $class_name  = isset($route[2][0]) ? $route[2][0] : null;
        $method_name = isset($route[2][1]) ? $route[2][1] : null;

        if(isset($route[1][2])) {
            array_shift($matches[0]);
            $params = array_combine($route[1][2], $matches[0]);
        }

        return array($class_name, $method_name, $params);
    }

    public function pathFor($name, $params = array())
    {
        $route_key = $this->mapNameRoute[$name];
        $pattern = array_search($route_key, $this->mapPatternRoute);

        return $this->reverse($pattern, $params);
    }

    // Input pattern '^/(foo)/(bar)$'
    // and input params array('baz', 'bat')
    // should return a path of '/baz/bat'
    // Warning: this will not test your params against the original pattern
    public function reverse($pattern, $params = array())
    {
        $count = 0;
        $path = preg_replace_callback(
            array('@\([^\)]+\)?@'), 
            function($matches) use ($params, &$count) {
                $replacement = $params[$count];
                $count++;
                return $replacement;
            },
            $pattern
            );
        $path = trim($path, '^$');

        return $path;
    }

    public function reverseTests()
    {
        $data = array(
            // pattern, params to substitute, expected result
            array('^/foo/$', array('foo', 'bar'), '/foo/'),
            array('^/foo/([a-z])/$', array('bar'), '/foo/bar/'),
            array('^/foo/(\d)/([0-9])/$', array(20, 14), '/foo/20/14/') 
        );
        foreach($data as $val) {
            $result = $this->reverse($val[0], $val[1]);
            $expected = $val[2];
            assert($result === $expected);
        }
    }
}